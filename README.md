# miniProject5-sm957



### Requirements

- Create a Rust AWS Lambda function (or app runner)
- Implement a simple service
- Connect to a database

### Step
- Create a new project      `cargo lambda newproject`
- Edit /src/main.rs file to add functionalities. My functionality is to add data to DynamoDB in table StuInfo. It has two fields, id and name. Add dependencies to Cargo.toml file.
- Build the application  `cargo lambda build --release`
- Create a new role "myrole" in IAM, and add `AmazonDynamoDBFullAccess`, `AWSLambda_FullAccess`, and `AWSLambdaBasicExecutionRole` permissions.
- Deploy it to AWS  
`cargo lambda deploy --region us-east-1 --iam-role <myrole>`  
- Ensure access id and access key is correct in `~/.aws/credentials` file
- After deploying, add a REST API Gateway for the lambda function. 
- Create a test event to sent a post request. Then check items of the database.

### Screenshots
- Create IAM role

![](img4.png)
- Lambda function and API Gateway

![](img2.png)
- Test event to sent a post request

![](img3.png)
- Updated StuInfo table in DynamoDB

![](img1.png)
